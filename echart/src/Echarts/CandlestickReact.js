import React from 'react'
import echarts from 'echarts/lib/echarts' 
import 'echarts/lib/chart/candlestick' //引入k线图
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/legend'
import 'echarts/lib/component/grid'
import 'echarts/lib/component/markLine'

export default class CandlestickReact extends React.Component {
  
  constructor(props) {
    super(props)
    this.initCandlestickReact = this.initCandlestickReact.bind(this)
  }
  
  initCandlestickReact() {
    const { option={} } = this.props //外部传入的data数据
    let myChart = echarts.init(this.ID) //初始化echarts
    
    //设置options
    myChart.setOption(option)
    window.onresize = function() {
      myChart.resize()
    }
  }
  
  componentDidMount() {
    this.initCandlestickReact()
  }
  
  componentDidUpdate() {
    this.initCandlestickReact()
  }
  
  render() {
    const { width="100%", height="400px" } = this.props
    return <div ref={ID => this.ID = ID} style={{width, height}}></div>
  }
}