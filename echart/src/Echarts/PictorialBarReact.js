import React from 'react'
import echarts from 'echarts/lib/echarts' //必须
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/legend'
import 'echarts/lib/component/grid'
import 'echarts/lib/chart/bar'
import 'echarts/lib/component/geo'
import 'echarts/lib/chart/lines'




export default class PictorialBarReact extends React.Component {
  
  constructor(props) {
    super(props)
    this.initPictorialBar = this.initPictorialBar.bind(this)
  }
  
  initPictorialBar() {
    const { option={} } = this.props //外部传入的data数据
    let myChart = echarts.init(this.ID) //初始化echarts

    //设置options
    myChart.setOption(option)
    window.onresize = function() {
      myChart.resize()
    }
  }
  
  componentDidMount() {
    this.initPictorialBar()
  }
  
  componentDidUpdate() {
    this.initPictorialBar()
  }
  
  render() {
    const { width="100%", height = '858px' } = this.props
    return <div ref={ID => this.ID = ID} style={{width, height}}></div>
  }
}