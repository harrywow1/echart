import React, { Component } from 'react';
import asyncComponent from './AsyncComponent'
import { pieOption, barOption, lineOption, scatterOption, mapOption, radarOption, candlestickOption, pieoOption, funnelOption,gaugeOption,pictorialBarOption } from './optionConfig/options'
const PieReact = asyncComponent(() => import(/* webpackChunkName: "Pie" */'./Echarts/PieReact'))  
const BarReact = asyncComponent(() => import(/* webpackChunkName: "Bar" */'./Echarts/BarReact')) 
const LineReact = asyncComponent(() => import(/* webpackChunkName: "Line" */'./Echarts/LineReact'))  //折线图组件
const ScatterReact = asyncComponent(() => import(/* webpackChunkName: "Scatter" */'./Echarts/ScatterReact'))  //散点图组件
const MapReact = asyncComponent(() => import(/* webpackChunkName: "Map" */'./Echarts/MapReact'))  
const RadarReact = asyncComponent(() => import(/* webpackChunkName: "Radar" */'./Echarts/RadarReact')) //雷达图组件  
const CandlestickReact = asyncComponent(() => import(/* webpackChunkName: "Candlestick" */'./Echarts/CandlestickReact')) //k线图组件
const PieoReact = asyncComponent(() => import(/* webpackChunkName: "Pieo" */'./Echarts/PieoReact')) 
const FunnelReact = asyncComponent(() => import(/* webpackChunkName: "Funnel" */'./Echarts/FunnelReact')) 
const GaugeReact = asyncComponent(() => import(/* webpackChunkName: "Gauge" */'./Echarts/GaugeReact')) //仪表组件
const PictoriaBarReact = asyncComponent(() => import(/* webpackChunkName: "PictoriaBar" */'./Echarts/PictorialBarReact')) //仪表组件
class App extends Component {
  render() {
    return (
       <div>
	      <h2>象形react组件实现</h2>
	      
	      <hr/>

        <h2>漏斗图</h2>
		    <FunnelReact option={funnelOption} />
		    <hr/>
		
        <h2>仪表图</h2>
		    <GaugeReact option={gaugeOption} />
		    <hr/>
		
        <h2>环形饼图</h2>
        <PieReact option={pieOption} height="485px"/>
        <hr/>
  
        <h2>柱状图</h2>
        <BarReact option={barOption} height="558px"/>
        <hr/>
        
        <h2>折线图</h2>
        <LineReact option={lineOption} height="558px"/>
        <hr/>
  
        <h2>散点图</h2>
        <ScatterReact option={scatterOption} height="650px"/>
        <hr/>
        
        <h2>地图</h2>
        <MapReact option={mapOption} height="558px" />
        <hr/>
  
        <h2>雷达图</h2>
        <RadarReact option={radarOption} height="800px"/>
        <hr/>
  
        <h2>k线图</h2>
        <CandlestickReact option={candlestickOption} height="800px"/>
        <hr/>
				
		    <h2>新饼图</h2>
		    <PieReact option={pieoOption} height="800px"/>
		    <hr/>
				
	      
		    
		
        </div>
    );
  }
}

export default App;
